size_t file_get_size(FILE *fp)
{
    fseek(fp, 0L, SEEK_END);
    size_t file_size = ftell(fp);
    rewind(fp);
    return file_size;
}


size_t buffer_fread(uint8_t *buffer, size_t bytes, FILE *fp)
{
    return fread(buffer, 1, bytes, fp);
}


size_t buffer_fwrite(uint8_t *buffer, size_t bytes, FILE *fp)
{
    return fwrite(buffer, 1, bytes, fp);
}


void print_buffer_ascii(char *buffer, unsigned int buffer_length)
{
    fwrite(buffer, buffer_length, 1, stdout);
    printf("\n");
}


void print_buffer_hex(char *s, unsigned int buffer_length)
{
    for(int ii = 0; ii < buffer_length; ii++) {
        printf("%02x ", (unsigned char) s[ii]);
    }
    printf("\n");
}