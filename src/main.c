#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <time.h>
#include "helper.c"


#define XOR_KEY_SIZE 512 // 4096 bits
uint8_t key_buffer[XOR_KEY_SIZE];


void generate_xor_key(char *password);
void encrypt(uint8_t* data_buffer, size_t data_size, uint8_t* key_buffer, size_t key_size);


int main(int argc, char **argv)
{
    if(argc != 2) {
        printf ("Usage: dxor [FILE]... \n");
        return 0;
    }


    FILE *fp = fopen(argv[1], "r+");
    if (fp == NULL) {
        printf ("Failed to open file \n");
        return 0;
    }


    char *password = getpass("Enter password: \n");
    if(password[0] == 0) {
        printf ("Password cannot be empty \n");
        return 0;
    }
    generate_xor_key(password);


    size_t file_size = file_get_size(fp);
    uint8_t *file_buffer = malloc(file_size);
    if (file_buffer == NULL) {
        printf ("Memory not allocated \n");
        return 0;
    }


    size_t read_size = buffer_fread(file_buffer, file_size, fp);
    if(read_size != file_size) {
        printf ("Failed file_buffer fill \n");
        return 0;
    }

    clock_t start = clock();


    encrypt(file_buffer, file_size, key_buffer, XOR_KEY_SIZE);


    clock_t  end = clock();
    //printf("time %lu \n",  end - start/1000000);
    printf("time %Lf \n", (long double)(end - start));
    printf("file size %lu \n", file_size);
    printf("%f Mb/s\n\n", ((((float)file_size/((float)(end - start)/(float)1000000))/(float)1024)/(float)1024));


    fclose(fp);
    fp = fopen(argv[1], "w");


    if(buffer_fwrite(file_buffer, file_size, fp) != file_size) {
        printf ("Failed to write back buffer \n");
        return 0;
    }


    return 0;
}


/**
 * Creates the key for encryption.
 * @todo replace with a more secure implementation.
 */
void generate_xor_key(char *password)
{
    size_t password_len = strlen(password);


    if (password_len == XOR_KEY_SIZE) {
        memcpy(key_buffer, password, XOR_KEY_SIZE); // potential bug, XOR_KEY_SIZE - 1 ??? so confusing
    } else if (password_len < XOR_KEY_SIZE) {
        memcpy(key_buffer, password, password_len);
        for (size_t ii = 0; ii < XOR_KEY_SIZE; ii++) {
            for (size_t ee = 0; ee < password_len; ee++) {
                size_t s_i = ii + ee;
                size_t d_i = s_i + password_len;
                if (s_i >= XOR_KEY_SIZE) {
                    s_i = s_i - XOR_KEY_SIZE;
                }
                if (d_i >= XOR_KEY_SIZE) {
                    d_i = d_i - XOR_KEY_SIZE;
                }

                key_buffer[d_i] = key_buffer[s_i] ^ (char)d_i;

                /**
                 * without this on some small passwords the buffer can zero out,
                 * probably not a fix, just makes this scenario highly unlikely to happen.
                 */
                if(ii > password_len + 8) {
                    if(key_buffer[d_i] < ii) {
                        key_buffer[key_buffer[d_i]] = key_buffer[key_buffer[d_i]] ^ key_buffer[d_i];
                    } else {
                        key_buffer[key_buffer[d_i] % (ii+2)] = key_buffer[key_buffer[d_i] % (ii+2)] ^ key_buffer[d_i];
                    }

                    if(key_buffer[s_i] < ii) {
                        key_buffer[key_buffer[s_i]] = key_buffer[key_buffer[s_i]] ^ key_buffer[s_i];
                    } else {
                        key_buffer[key_buffer[s_i] % (ii+2)] = key_buffer[key_buffer[s_i] % (ii+2)] ^ key_buffer[s_i];
                    }
                }
            }
        }
    } else if (password_len > XOR_KEY_SIZE) {
        printf ("not implemented !!! \n");
        exit(0);
    };
}


/**
 * key_size - must be dividable by 64
 * @todo key add key validation
 * @todo compress data before
 */
void encrypt(uint8_t* data_buffer, size_t data_size, uint8_t* key_buffer, size_t key_size)
{
    /* I was thinking about using Duff's device but its so ugly syntax wise i chosen not to */


    size_t key_index = 0;
    size_t buffer_index = 0;
    size_t const unroll_factor = 16;
    size_t const cast_factor = sizeof(uint64_t) / sizeof(uint8_t); // 8


    uint64_t *data_buffer_uint64_t = (uint64_t *)data_buffer;
    uint64_t *key_buffer_uint64_t  = (uint64_t *)key_buffer;


    size_t fast_loop_limit = ((size_t)data_size / (cast_factor * unroll_factor)) * unroll_factor;
    size_t key_size_uint64_t = key_size / cast_factor;
    size_t remainder_start = fast_loop_limit * cast_factor;


    for (buffer_index = 0; buffer_index < fast_loop_limit; buffer_index+= unroll_factor) {
        if (key_index >= key_size_uint64_t) {
            key_index = 0;
        }
        data_buffer_uint64_t[buffer_index+0] ^= key_buffer_uint64_t[key_index+0];
        data_buffer_uint64_t[buffer_index+1] ^= key_buffer_uint64_t[key_index+1];
        data_buffer_uint64_t[buffer_index+2] ^= key_buffer_uint64_t[key_index+2];
        data_buffer_uint64_t[buffer_index+3] ^= key_buffer_uint64_t[key_index+3];
        data_buffer_uint64_t[buffer_index+4] ^= key_buffer_uint64_t[key_index+4];
        data_buffer_uint64_t[buffer_index+5] ^= key_buffer_uint64_t[key_index+5];
        data_buffer_uint64_t[buffer_index+6] ^= key_buffer_uint64_t[key_index+6];
        data_buffer_uint64_t[buffer_index+7] ^= key_buffer_uint64_t[key_index+7];
        data_buffer_uint64_t[buffer_index+8] ^= key_buffer_uint64_t[key_index+8];
        data_buffer_uint64_t[buffer_index+9] ^= key_buffer_uint64_t[key_index+9];
        data_buffer_uint64_t[buffer_index+10] ^= key_buffer_uint64_t[key_index+10];
        data_buffer_uint64_t[buffer_index+11] ^= key_buffer_uint64_t[key_index+11];
        data_buffer_uint64_t[buffer_index+12] ^= key_buffer_uint64_t[key_index+12];
        data_buffer_uint64_t[buffer_index+13] ^= key_buffer_uint64_t[key_index+13];
        data_buffer_uint64_t[buffer_index+14] ^= key_buffer_uint64_t[key_index+14];
        data_buffer_uint64_t[buffer_index+15] ^= key_buffer_uint64_t[key_index+15];
        key_index+= unroll_factor;
    }


    key_index = key_index * cast_factor;
    for (buffer_index = remainder_start; buffer_index < data_size; buffer_index++) {
        if (key_index >= key_size) {
            key_index = 0;
        }
        data_buffer[buffer_index] ^= key_buffer[key_index];
        key_index++;
    }
}